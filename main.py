class Persona:
    def __init__(self, nombre, cantidad_celulas, temperatura=36.5):
        self.nombre = nombre
        self.cant_celulas = cantidad_celulas
        self.temperatura = temperatura
        self.enfermedades = []

    def enfermar(self, enfermedad):
        self.enfermedades.append(enfermedad)

    def pasar_dia(self):
        for enfermedad in self.enfermedades:
            enfermedad.pasar_dia(self)
        if self.cant_celulas > 1000000 and self.temperatura < 50:
            print(self.nombre, "vive un día más")
        else:
            print(self.nombre, "no sobrevivió")

    def disminuir_celulas(self, cantidad):
        self.cant_celulas = max(0, self.cant_celulas - cantidad)

    def aumentar_temperatura(self, temp):
        self.temperatura += temp

    def medicar(self, dosis):
        print(self.nombre, "recibió una dosis de", dosis)
        index = 0
        for enfermedad in self.enfermedades:
            if enfermedad.medicar(dosis) == 0:
                self.enfermedades.pop(index)
            else:
                index += 1

    def descripcion(self):
        print(self.nombre, "[T: {}, Cels: {}]".format(self.temperatura, self.cant_celulas), end=". ")
        for enfermedad in self.enfermedades:
            enfermedad.descripcion()


class Enfermedad():
    def __init__(self, nombre, celulas_amenazadas):
        self.nombre = nombre
        self.celulas_amenazadas = celulas_amenazadas

    def pasar_dia(self, persona: Persona):
        pass

    def medicar(self, dosis):
        self.celulas_amenazadas = max(0, self.celulas_amenazadas - 15 * dosis)
        if self.celulas_amenazadas == 0:
            print("Está mejorando")
        return self.celulas_amenazadas

    def descripcion(self):
        print("Tiene {} ({} cels.). ".format(self.nombre, self.celulas_amenazadas), end="")


class EnfermedadInfecciosa(Enfermedad):
    def pasar_dia(self, persona: Persona):
        persona.aumentar_temperatura(self.celulas_amenazadas / 1000)
        self.celulas_amenazadas = self.celulas_amenazadas * 2


class EnfermedadAutoinmune(Enfermedad):
    def pasar_dia(self, persona: Persona):
        persona.disminuir_celulas(self.celulas_amenazadas)
        self.celulas_amenazadas = self.celulas_amenazadas / 2


if __name__ == '__main__':
    per = Persona("Gonzalo", 3000000, temperatura=36)
    per.enfermar(EnfermedadInfecciosa("malaria", 5000))
    per.enfermar(EnfermedadAutoinmune("lupus", 10000))
    per.descripcion()
    per.pasar_dia()
    per.medicar(10)
    per.descripcion()
    per.medicar(600)
    per.pasar_dia()
    per.descripcion()
    per.pasar_dia()
    per.descripcion()

